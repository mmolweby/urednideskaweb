import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EdeskaService {

  //API URL
  apiUrlBase = "https://api.olomouc.eu/edeska/"

  apiUrl_kategorie = this.apiUrlBase + "kategorie"
  apiUrl_vyveseneZaznamy = this.apiUrlBase + "vyvesene-zaznamy"

  constructor(private http: HttpClient) { }

  public ziskatKategorie(): Observable<kategorie[]> {
    let dotaz = {

    }

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post<any>(this.apiUrl_kategorie, JSON.stringify(dotaz), httpOptions).pipe(
      map(res => {
        console.log("ziskavam kategorie z odpovedi api", res)
        if (res.hlavicka.uspech && res.kategorie) {
          return (res.kategorie)
        }
      })
    );

  }

  ziskatVyveseneZaznamy(hledanyText: string, kategorie: number, preskocit: number, limit: number): Observable<vyvesenyZaznam[]> {
    let dotaz = {
      hledanyText: hledanyText,
      kategorie: kategorie,
      preskocit: preskocit,
      limit: limit,
    }

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post<any>(this.apiUrl_vyveseneZaznamy, JSON.stringify(dotaz), httpOptions).pipe(
      map(res => {
        console.log("ziskavam vyvesene zaznamy z odpovedi api", res)
        if (res.hlavicka.uspech && res.zaznamy) {
          return (res.zaznamy)
        }
      })
    );
  }


}

export class vyvesenyZaznam {
  id: number
  vyvesenoOd: Date
  vyvesenoDo: Date
  kategorie: number
  nazev: string
  znacka: string
  puvodce: string
  prilohy: prilohaVyvesenehoZaznamu[]

}

export class prilohaVyvesenehoZaznamu {
  id: number
  url: string
  nazevSouboru: string
  velikost: number
}

export class kategorie {
  id: number
  nazev: string
}

