import { TestBed } from '@angular/core/testing';

import { EdeskaService } from './edeska.service';

describe('EdeskaService', () => {
  let service: EdeskaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EdeskaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
