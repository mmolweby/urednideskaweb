import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HlavickaComponent } from './hlavicka.component';

describe('HlavickaComponent', () => {
  let component: HlavickaComponent;
  let fixture: ComponentFixture<HlavickaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HlavickaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HlavickaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
