import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { EdeskaService, kategorie } from 'src/app/services/edeska.service';

@Component({
  selector: 'app-filtrovany-prehled',
  templateUrl: './filtrovany-prehled.component.html',
  styleUrls: ['./filtrovany-prehled.component.scss']
})
export class FiltrovanyPrehledComponent implements OnInit {

  kategorie: kategorie[] = []

  zaznamy: zaznam[] = [
    {
      id: 1,
      nazev: "Oznámení o nálezu zvířete - pes",
      kategorie: "Ztráty a nálezy",
      vyvesitOd: new Date("2021-04-14"),
      vyvesitDo: new Date("2021-04-15"),
      znacka: "SMOL/256516/2017/MPO/PDO/PS/Kor 2017/038635",
      puvodce: "Statutární město Olomouc Horní náměstí",
      prilohy: [
        {
          nazevSouboru: "test.pdf",
          velikost: 100225,
          url: "http://ags1e86g48sag",
        },
        {
          nazevSouboru: "test.pdf",
          velikost: 400125,
          url: "http://ags1e86g48sag",
        }
      ]
    },
    {
      id: 1,
      nazev: "RMO ze dne 31. 10. 2017 - záměry prodat, vypůjčit, pronajmout nemovité věci, popř. jejich části",
      kategorie: "Záměry nakládání s nemovitým majetkem obce",
      vyvesitOd: new Date("2021-04-14"),
      vyvesitDo: new Date("2021-04-14"),
      znacka: "SMOL/254027/2017/OMAJ/MR/Tom",
      puvodce: "Valenta Jan",
      prilohy: [
        {
          nazevSouboru: "test.pdf",
          velikost: 35112548,
          url: "http://ags1e86g48sag",
        }
      ]
    }
  ]

  vybranyZaznam: zaznam

  filtrKategorie: number

  constructor(private _edeska: EdeskaService) { }

  ngOnInit(): void {
    this.nacistKategorieZApi()
  }

  nacistKategorieZApi() {
    this._edeska.ziskatKategorie().subscribe(res=>{
      this.kategorie = res
    })

  }

  vybranaPolozka(zaznam: zaznam) {
    return zaznam === this.vybranyZaznam ? true : false
  }

  rozkliknoutZaznam(zaznam: zaznam) {
    this.vybranyZaznam = zaznam
  }

  pocetPriloh(z: zaznam): number {
    return z && z.prilohy ? z.prilohy.length : null
  }

  velikostSouboruLidsky(velikostBajty: number) {
    let vystup = velikostBajty + "b"
    if (velikostBajty > 1000000) {
      vystup = Math.round(velikostBajty / 1000000) + "mb"
    } else if (velikostBajty > 1000) {
      vystup = Math.round(velikostBajty / 1000) + "kb"
    }
    return vystup
  }

  tooltipKTlacitkuSouboru(p:prilohaZaznamu):string {
    return "Klepnutím soubor otevřít"

  }





}


export class zaznam {
  id: number
  nazev: string
  kategorie: string
  vyvesitOd: Date
  vyvesitDo: Date
  znacka: string
  puvodce: string
  prilohy: prilohaZaznamu[]
}

export class prilohaZaznamu {
  nazevSouboru: string
  velikost: number
  url: string
}