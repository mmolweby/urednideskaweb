import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrovanyPrehledComponent } from './filtrovany-prehled.component';

describe('FiltrovanyPrehledComponent', () => {
  let component: FiltrovanyPrehledComponent;
  let fixture: ComponentFixture<FiltrovanyPrehledComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiltrovanyPrehledComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrovanyPrehledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
